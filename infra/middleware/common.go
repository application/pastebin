package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// CorsFilter 跨域过滤器
func CorsFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Authorization,X-CSRF-Token,Token,session")
		c.Header("Access-Control-Allow-Methods", "*")
		c.Header("Access-Control-Expose-Headers", "*")
		if c.Request.Method == "OPTIONS" {
			c.String(http.StatusNoContent, "OPTIONS")
			return
		}
	}
}

// ResponseJsonHeader 统一设置返回json格式数据
func ResponseJsonHeader() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
	}
}
