package router

import (
	"fmt"
	"gitea.com/iota/iota/core"
	"gitea.com/iota/mdbc"
	"github.com/gin-gonic/gin"
	"os"
	"pastebin/config"
	"pastebin/infra/middleware"
	"pastebin/internal/controller"

	"github.com/gin-contrib/static"
)

type Router struct{}

func NewRouter() *Router {
	return &Router{}
}

// Register 注册路由
func (receiver *Router) Register() error {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Recovery(), middleware.CorsFilter())
	router.Use(static.Serve("/", static.LocalFile("./webui/dist", true)))
	// 从这里开始实例化路由注册器
	register := core.NewRegister()
	register.BindRouteMap(controller.PastebinAPIGroupRouterMap)
	register.RegisterStruct(router.Group("", middleware.ResponseJsonHeader()), &controller.PastebinAPI{})

	router.GET("/raw/:short_key", controller.RawRecord)

	router.NoRoute(func(ctx *gin.Context) {
		ctx.File("./webui/dist/index.html")
	})

	cfg := config.Get()
	_, _ = fmt.Fprintf(os.Stdout, "api server: %s\n", cfg.ServerListen)
	return router.Run(cfg.ServerListen)
}

// Init 注册前的路由初始化
func (receiver Router) Init() error {
	controller.RegisterError()
	config.Get().Mongo.RegistryBuilder = mdbc.RegisterTimestampCodec(nil)
	mongo, err := mdbc.ConnInit(config.Get().Mongo)
	if err != nil {
		return err
	}
	mdbc.Init(mongo).InitDB(mongo.Database(config.Get().Mongo.DBName))
	return nil
}
