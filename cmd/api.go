package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"pastebin/internal/router"
)

var (
	apiServerCommand = &cobra.Command{
		Use:   "api",
		Short: "start api server",
		Long:  "start api server",
		Run: func(cmd *cobra.Command, args []string) {
			router := router.NewRouter()
			if err := router.Init(); err != nil {
				_, _ = fmt.Fprintf(os.Stderr, "项目初始化失败: %+v\n", err)
				return
			}

			go router.Register()
			select {}
		},
	}
)
