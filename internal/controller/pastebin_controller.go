package controller

import (
	"fmt"
	"pastebin/model"
	"pastebin/toolkit"
	"time"

	"gitea.com/iota/iota/core"
	"gitea.com/iota/iota/utils"
	"gitea.com/iota/mdbc"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type PastebinAPI struct{}

// IDE: PastebinAPI implemented PastebinAPIImpl interface
var _ PastebinAPIImpl = (*PastebinAPI)(nil)

// Bind 绑定路由组名称 默认service名称 请不要擅自修改
func (receiver *PastebinAPI) Bind() string {
	return "PastebinAPI"
}

// Ping echo
func (receiver *PastebinAPI) Ping(ctx *core.Context, req *PingReq) (resp *PingResp, err error) {
	resp = new(PingResp)

	resp.Pong = req.Ping

	return resp, nil
}

// List 罗列pastebin列表
func (receiver *PastebinAPI) List(ctx *core.Context, req *ListReq) (resp *ListResp, err error) {
	resp = new(ListResp)

	if req.Page <= 0 {
		req.Page = 1
	}

	if req.PageSize <= 0 {
		req.PageSize = 10
	}

	offset := (req.Page - 1) * req.PageSize

	if offset < 0 {
		return resp, core.CreateError(ParamsInvalid)
	}

	scope := new(model.ModelPastebin).GetScope()

	var list []*model.ModelPastebin
	var total int64
	if err := scope.Find().SetContext(ctx).WithCount(&total).SetLimit(req.PageSize).SetSkip(offset).GetList(&list); err != nil {
		logrus.Errorf("get err: %+v", err)
		return resp, err
	}

	resp.CurrentPage = req.Page
	resp.TotalPage = utils.DivisionRoundingUp(total, req.Page)
	resp.List = list
	return resp, nil
}

// Get 获取一个记录
func (receiver *PastebinAPI) Get(ctx *core.Context, req *GetReq) (resp *GetResp, err error) {
	resp = new(GetResp)

	scope := new(model.ModelPastebin).GetScope()

	var record model.ModelPastebin
	if err := scope.FindOne().SetContext(ctx).SetFilter(bson.M{
		model.ModelPastebinField.GetShortKeyField(): req.Key,
	}).Get(&record); err != nil {
		logrus.Errorf("get err: %+v", err)
		return resp, err
	}

	if record.Password != "" && record.Password != req.Password {
		return resp, core.CreateError(PasswordNotMatch)
	}

	resp.Record = &record
	return resp, nil
}

// Add 添加一个记录
func (receiver *PastebinAPI) Add(ctx *core.Context, req *AddReq) (resp *AddResp, err error) {
	resp = new(AddResp)

	if req.Record == nil {
		return resp, core.CreateError(ParamsInvalid)
	}

	scope := new(model.ModelPastebin).GetScope()

	record := req.Record

	// 必须重写id
	record.Id = toolkit.RandomString()
	// 置一下过期时间
	record.ExpiredAt = timestamppb.New(toolkit.GetExpiredAtTime(record.LifeCycle))
	// 置一下短链接
	if record.ShortKey != "" {
		// 检测是否已经存在
		exist, err := scope.FindOne().SetContext(ctx).SetFilter(bson.M{
			model.ModelPastebinField.GetShortKeyField(): record.ShortKey,
		}).IsExist()
		if err != nil {
			logrus.Errorf("get err: %+v", err)
			return resp, err
		}
		if exist {
			return resp, core.CreateError(ShortKeyExist)
		}
	} else {
		record.ShortKey, err = toolkit.GenerateShortKey(record.Content)
		if err != nil {
			logrus.Errorf("get err: %+v", err)
			return resp, err
		}
	}

	var timeNow = time.Now().Unix()
	record.CreatedAt = timeNow
	record.UpdatedAt = timeNow

	if _, err := scope.Insert().SetContext(ctx).One(record); err != nil {
		logrus.Errorf("get err: %+v", err)
		return resp, err
	}

	resp.Record = record
	return resp, nil
}

// Del 删除一个记录
func (receiver *PastebinAPI) Del(ctx *core.Context, req *DelReq) (resp *DelResp, err error) {
	resp = new(DelResp)

	scope := new(model.ModelPastebin).GetScope()

	if _, err := scope.Delete().SetContext(ctx).SetFilter(bson.M{
		model.ModelPastebinField.GetShortKeyField(): req.Key,
	}).One(); err != nil {
		logrus.Errorf("get err: %+v", err)
		return resp, err
	}

	return resp, nil
}

// Set 修改记录
func (receiver *PastebinAPI) Set(ctx *core.Context, req *SetReq) (resp *SetResp, err error) {
	resp = new(SetResp)

	if req.Record == nil {
		return resp, fmt.Errorf("get record empty")
	}

	scope := new(model.ModelPastebin).GetScope()

	record := req.Record

	// 置一下过期时间
	record.ExpiredAt = timestamppb.New(toolkit.GetExpiredAtTime(record.LifeCycle))
	// 置一下短链接
	if record.ShortKey != "" {
		// 检测是否已经存在 不能篡改
		exist, err := scope.FindOne().SetContext(ctx).SetFilter(bson.M{
			model.ModelPastebinField.GetIdField():       record.Id,
			model.ModelPastebinField.GetShortKeyField(): record.ShortKey,
		}).IsExist()
		if err != nil {
			logrus.Errorf("get err: %+v", err)
			return resp, err
		}
		if !exist {
			return resp, fmt.Errorf("short key cant reset")
		}
	} else {
		record.ShortKey, err = toolkit.GenerateShortKey(record.Content)
		if err != nil {
			logrus.Errorf("get err: %+v", err)
			return resp, err
		}
	}

	if req.Password != "" {
		// 检测是否和以前一致 不一致不能修改
		exist, err := scope.FindOne().SetContext(ctx).SetFilter(bson.M{
			model.ModelPastebinField.GetShortKeyField(): record.ShortKey,
			model.ModelPastebinField.GetPasswordField(): req.Password,
		}).IsExist()
		if err != nil {
			logrus.Errorf("get err: %+v", err)
			return resp, err
		}
		if !exist {
			return resp, core.CreateError(PasswordNotMatch)
		}
	}

	setData := mdbc.Struct2MapOmitEmptyWithBsonTag(record)

	if _, err := scope.Update().SetContext(ctx).SetFilter(bson.M{
		model.ModelPastebinField.GetShortKeyField(): record.ShortKey,
		model.ModelPastebinField.GetIdField():       record.Id,
	}).One(setData); err != nil {
		logrus.Errorf("get err: %+v", err)
		return resp, err
	}

	resp.Record = record
	return resp, nil
}

// BatchDel 批量删除
func (receiver *PastebinAPI) BatchDel(ctx *core.Context, req *BatchDelReq) (resp *BatchDelResp, err error) {
	resp = new(BatchDelResp)

	scope := new(model.ModelPastebin).GetScope()

	if _, err := scope.Delete().SetContext(ctx).SetFilter(bson.M{
		model.ModelPastebinField.GetShortKeyField(): bson.M{
			"$in": req.Keys,
		},
	}).Many(); err != nil {
		logrus.Errorf("get err: %+v", err)
		return resp, err
	}

	return resp, nil
}
