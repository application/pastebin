package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"pastebin/model"
)

func RawRecord(c *gin.Context) {
	key, exist := c.Params.Get("short_key")
	if !exist {
		c.String(http.StatusOK, "key empty")
	}

	var record model.ModelPastebin
	scope := new(model.ModelPastebin).GetScope()
	if err := scope.FindOne().SetContext(c).SetFilter(bson.M{
		model.ModelPastebinField.GetShortKeyField(): key,
	}).Get(&record); err != nil {
		logrus.Errorf("get err: %+v", err)
		c.String(http.StatusOK, err.Error())
		return
	}

	c.String(http.StatusOK, record.Content)
}
