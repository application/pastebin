module pastebin

go 1.16

require (
	gitea.com/iota/iota v0.0.0-20220102112836-c0cd94d84906
	gitea.com/iota/mdbc v0.0.0-20220102150048-0c49509cd674
	gitea.com/iota/toolkit v0.0.0-20211216085837-d28478c33804 // indirect
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/gin-contrib/static v0.0.1
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.3.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	go.mongodb.org/mongo-driver v1.8.1
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
