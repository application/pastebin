package toolkit

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"math"
	"math/rand"
	"strconv"
	"time"
)

// MD5 取md5摘要
func MD5(s string) string {
	h := md5.New()
	h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}

// RandomString 获取一个随机字符串
func RandomString() string {
	return MD5(uuid.New().String())
}

// RandomNumberString 获取一个随机数字字符串
func RandomNumberString() string {
	rand.NewSource(int64(time.Nanosecond))
	f := 10000 + rand.Intn(89999)
	var now = time.Now().UnixNano() / 1e6

	return fmt.Sprintf("%d%d", now, f)
}

func GenerateShortKey(content string) (string, error) {
	var _t = fmt.Sprintf("%s.%d.", content, time.Now().Unix())
	var h = md5.New()
	_, _ = h.Write([]byte(_t))
	var digest = hex.EncodeToString(h.Sum(nil))[8:24]
	front, err := strconv.ParseInt(digest[:len(digest)/2], 16, 64)
	if err != nil {
		logrus.Errorf("generate sk err: %v", err)
		return "", err
	}

	end, err := strconv.ParseInt(digest[len(digest)/2:], 16, 64)
	if err != nil {
		logrus.Errorf("gen sk err: %v", err)
		return "", err
	}

	frontDigest := Encode62(front)
	endDigest := Encode62(end)
	return fmt.Sprintf("%s%s", frontDigest, endDigest), nil
}

// characters used for conversion
const alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

// Encode62 converts number to base62
func Encode62(number int64) string {
	if number == 0 {
		return string(alphabet[0])
	}

	chars := make([]byte, 0)

	length := int64(len(alphabet))

	for number > 0 {
		result := number / length
		remainder := number % length
		chars = append(chars, alphabet[remainder])
		number = result
	}

	for i, j := 0, len(chars)-1; i < j; i, j = i+1, j-1 {
		chars[i], chars[j] = chars[j], chars[i]
	}

	return string(chars)
}

// Decode62 converts base62 token to int64
func Decode62(token string) int64 {
	var number int64
	var idx = 0.0
	chars := []byte(alphabet)

	charsLength := float64(len(chars))
	tokenLength := float64(len(token))

	for _, c := range []byte(token) {
		power := tokenLength - (idx + 1)
		index := int64(bytes.IndexByte(chars, c))
		number += index * int64(math.Pow(charsLength, power))
		idx++
	}

	return number
}
