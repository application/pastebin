import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import("@/views/Home.vue")
    },
    {
        path: '/s/:sk',
        name: 'Show',
        component: () => import("@/views/Show.vue")
    },
    {
        path: '/edit/:sk',
        name: 'Edit',
        component: () => import("@/views/Edit.vue")
    },
    {
        path: '/archive',
        name: 'Archive',
        component: () => import("@/views/Archive.vue")
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
