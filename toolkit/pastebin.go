package toolkit

import (
	"pastebin/model"
	"time"
)

// GetExpiredAtTime 获取记录失效时间
func GetExpiredAtTime(cycle model.LifeCycle) time.Time {
	var now = time.Now()
	switch cycle {
	case model.LifeCycle_OneDay:
		now = now.Add(time.Hour * 24)
	case model.LifeCycle_OneWeek:
		now = now.Add(time.Hour * 24 * 7)
	case model.LifeCycle_OneMonth:
		now = now.Add(time.Hour * 24 * 31)
	case model.LifeCycle_HalfYear:
		now = now.Add(time.Hour * 24 * 180)
	case model.LifeCycle_OneYear:
		now = now.Add(time.Hour * 24 * 365)
	case model.LifeCycle_Forever:
		now = now.Add(time.Hour * 24 * 3650) // 10年
	default:
		now = now.Add(time.Hour * 24)
	}
	return now
}
