package cmd

import (
	"fmt"
	nested "github.com/antonfisher/nested-logrus-formatter"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"pastebin/config"
	"runtime"
	"strings"
)

var (
	rootCmd = &cobra.Command{}
	cfgFile string
)

func Execute() {
	// 预加载配置文件
	loadConfig()
	if err := rootCmd.Execute(); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func init() {
	var getServeDir = func(path string) string {
		var run, _ = os.Getwd()
		return strings.Replace(path, run, ".", -1)
	}
	var formatter = &nested.Formatter{
		NoColors:        false,
		HideKeys:        true,
		TimestampFormat: "2006-01-02 15:04:05",
		CallerFirst:     true,
		CustomCallerFormatter: func(f *runtime.Frame) string {
			s := strings.Split(f.Function, ".")
			funcName := s[len(s)-1]
			return fmt.Sprintf(" [%s:%d][%s()]", getServeDir(f.File), f.Line, funcName)
		},
	}
	logrus.SetFormatter(formatter)
	logrus.SetReportCaller(true)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "config_local.yaml", "config file")
	rootCmd.AddCommand(apiServerCommand) // API服务
}

func loadConfig() {
	// 初始化配置文件
	config.New(cfgFile)
	conf := config.Get()
	if err := conf.Load(); err != nil {
		panic(err)
	}
}
