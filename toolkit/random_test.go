package toolkit

import (
	"fmt"
	"testing"
)

func TestGenerateShortKey(t *testing.T) {
	body, err := GenerateShortKey("hello world")
	if err != nil {
		panic(err)
	}

	fmt.Println(body)
}
